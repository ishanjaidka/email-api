//import modules installed at the previous step. We need them to run Node.js server and send emails
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    encryption: "tls",
    host: "smtp.office365.com",
    port: 587,
    auth: {
        user: 'z3521168@ad.unsw.edu.au',
        pass: 'Maisha@123'
    }
});

// create a new Express application instance 
const app = express();

//configure the Express middleware to accept CORS requests and parse request body into JSON
app.use(cors());
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb' }));

//start application server on port 3000
app.listen(3000, () => {
    console.log("The server started on port 3000");
});

// define a sendmail endpoint, which will send emails and response with the corresponding status
app.post("/sendmail", (req, res) => {
    console.log("request came");
    let user = req.body;
    sendMailToHeadOffice(user, (err, info) => {
        if (err) {
            console.log(err);
            res.status(400);
            res.send({ error: "Failed to send email" });
        } else {
            console.log("Email has been sent");
            res.send(info);
        }
    });
});

// define a sendmail endpoint, which will send emails and response with the corresponding status
app.post("/spo/sendmail", (req, res) => {
    console.log("request came");
    let user = req.body;
    sendMailToSPO(user, (err, info) => {
        if (err) {
            res.status(400);
            res.send({ error: "Failed to send email" });
        } else {
            console.log("Email has been sent");
            return res.send(info);
        }
    });
});

const sendMailToHeadOffice = (user, callback) => {
    let body = JSON.parse(user.body);
    const mailOptions = {
        from: 'i.jaidka@adfa.edu.au',
        to: 'ishan.jaidka@gmail.com',
        subject: "EPO Overview Report for Year - " + body.Year + ' Quarter - ' + body.Quarter,
        attachments: [{ // filename and content type is derived from path
            filename: 'EPO Report Overview.pdf',
            path: body.PdfPath
        }],
        html: body.Description
    };

    transporter.sendMail(mailOptions, callback);
}

const sendMailToSPO = (user, callback) => {
    let body = JSON.parse(user.body);
    const mailOptions = {
        from: 'i.jaidka@adfa.edu.au',
        to: 'ishan.jaidka@gmail.com',
        subject: "Non-Compliant EPO Report for Year - " + body.Year + ' Quarter - ' + body.Quarter,
        html: body.Description + '<br><br>' + body.Table
    };

    transporter.sendMail(mailOptions, callback);
}