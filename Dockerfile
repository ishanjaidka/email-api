FROM node:11-alpine

WORKDIR /app/
COPY package*.json /app/

RUN npm install

COPY . /app

EXPOSE 3000

# Add user "docker"
RUN addgroup --gid 1024 docker
RUN adduser -u 1024 docker -D -g "" -G docker
USER docker

CMD ["node", "server.js"]