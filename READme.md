Follow the steps below to run this API
It uses Node server

Step 1: Clone this Repo to your local computer
Step 2: Go to src directory by using "cd email-api"
Step 3: Run "npm install"
Step 4: Run "node server.js"

API will run on the port "3000"